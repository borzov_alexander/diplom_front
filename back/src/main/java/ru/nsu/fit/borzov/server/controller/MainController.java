package ru.nsu.fit.borzov.server.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.borzov.server.model.TaskEntity;
import ru.nsu.fit.borzov.server.view.TaskView;
import io.swagger.v3.oas.annotations.Operation;
import ru.nsu.fit.borzov.server.service.MainService;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MainController {
    //Logger logger = LoggerFactory.getLogger(LoggingController.class);
    private final MainService service;

    @Operation(summary = "ping")
    @PostMapping("ping")
    public boolean ping() {
        return true;
    }

    @PostMapping("addInQueue/{port}")
    public void getInQueue(HttpServletRequest httpServletRequest, /*@RequestParam("port") int port,*/ @PathVariable int port) throws Exception {
        String clientIp = httpServletRequest.getRemoteAddr();
        //FIXME:нужна проверка, если уже в очереди, то не добавлять.
        service.addInQueue(clientIp, port);
    }

    @GetMapping("checkClientStatus/{port}")
    public TaskEntity checkClientStatus(HttpServletRequest httpServletRequest, @PathVariable int port) {
        String clientIp = httpServletRequest.getRemoteAddr();
        return service.checkClientStatus(clientIp, port);
        //return service.checkStatus(clientIp, port);
    }

    @GetMapping("checkTaskStatus/{id}")
    public TaskEntity checkTaskStatus(@PathVariable UUID id) {
        return service.checkTaskStatus(id);
        //return service.checkStatus(clientIp, port);
    }

    @GetMapping("checkTaskStatus/{id}/{port}")
    public TaskEntity checkTaskStatus(@PathVariable UUID id, HttpServletRequest httpServletRequest, @PathVariable int port) {
        String clientIp = httpServletRequest.getRemoteAddr();
        return service.checkTaskStatus(id, clientIp, port);
        //return service.checkStatus(clientIp, port);
    }

    @PostMapping("addTask/{port}")
    public UUID addTask(HttpServletRequest httpServletRequest, @PathVariable int port, @RequestBody TaskView taskView) {
        String clientIp = httpServletRequest.getRemoteAddr();
        return service.addTask(clientIp, port, taskView);
        //return service.checkStatus(clientIp, port);
    }
    @DeleteMapping("deleteTask/{id}")
    public void deleteTask(@PathVariable UUID id) {
        service.deleteTask(id);
    }
}

