package ru.nsu.fit.borzov.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "uuid NOT NULL DEFAULT uuid_generate_v1()")
    UUID id;

    int port;

    String ip;


    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="task_id", columnDefinition="uuid")
    TaskEntity task;

    @Basic
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date timestamp = new java.util.Date();

}
