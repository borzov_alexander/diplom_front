package ru.nsu.fit.borzov.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
public class Row {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "uuid NOT NULL DEFAULT uuid_generate_v1()")
    UUID id;

    public int operation; //TODO: enum

    @ElementCollection
    public List<Integer> argsNumbers = new ArrayList<>();

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    TaskEntity task;
}
