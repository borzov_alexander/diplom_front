package ru.nsu.fit.borzov.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import ru.nsu.fit.borzov.server.model.variable.Matrix;
import ru.nsu.fit.borzov.server.model.variable.Variable;

import javax.persistence.*;
import java.util.*;
import java.util.UUID;

@Getter
@Setter
//@ToString
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TaskEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "uuid NOT NULL DEFAULT uuid_generate_v1()")
    UUID id;

    int usersCount;

    @Transient
    int currUserNumber;

    @OneToMany(mappedBy = "task", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<ClientEntity> clients = new ArrayList<>();

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private java.util.Date timestamp = new java.util.Date();

    boolean isStarted = false;

    //TODO:всё что ниже может быть тупо BLOB
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "task")
    List<Variable> context = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "task")
    List<Row> code = new ArrayList<>();
}
