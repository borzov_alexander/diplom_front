package ru.nsu.fit.borzov.server.model.variable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@Embeddable
//@DiscriminatorValue(value="M")
public class Matrix {

    @ElementCollection
    @Column(nullable = true)
    List<Double> data = new ArrayList<>();

    @Column(nullable = true)
    Integer width;

    @Column(nullable = true)
    Integer height;

    //String DTYPE;
}
