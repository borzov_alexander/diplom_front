package ru.nsu.fit.borzov.server.model.variable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.borzov.server.model.TaskEntity;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
//@DiscriminatorValue(value = "V")
//@DiscriminatorColumn(name = "DTYPE")
public class Variable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "uuid NOT NULL DEFAULT uuid_generate_v1()")
    UUID id;


    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    TaskEntity task;

    //String DTYPE;

    @Embedded
    MyDouble doubleObj;

    @Embedded
    Matrix matrix;
}
