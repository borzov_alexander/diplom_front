package ru.nsu.fit.borzov.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.borzov.server.model.ClientEntity;
import ru.nsu.fit.borzov.server.model.TaskEntity;

import java.util.*;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, UUID> {
    ClientEntity findFirstByIpAndPort(String ip, int port);
    List<ClientEntity> findAllByTask(TaskEntity task);
}
