//package ru.nsu.fit.borzov.server.repository;
//
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import ru.nsu.fit.borzov.server.view.ClientInfo;
//import ru.nsu.fit.borzov.server.view.TeamInfo;
//
//import java.util.*;
//
//@Repository
//@RequiredArgsConstructor(onConstructor = @__(@Autowired))
//public class MainRepository {
//    //private final LinkedList<ClientInfo> pendingClients;
//    private final HashMap<ClientInfo, TeamInfo> clientsInWork;
//
//    private final LinkedList<ClientInfo> queueOfWorkers;
//    private final Map<ClientInfo,TeamInfo> teamForClient;
//    private final Set<TeamInfo> teams;
//    private final LinkedList<TeamInfo> queueOfTeams;
//
//    private final int N = 2;
//    public TeamInfo checkStatus(ClientInfo client) {
//        synchronized (teamForClient) {
//            return teamForClient.get(client);
//        }
//    }
//    public TeamInfo updateStatusAndGetTeam(ClientInfo client) {
//        //TeamInfo team = clientsInWork.remove(client);
//        TeamInfo team = null;
//            for(Iterator<Map.Entry<ClientInfo,TeamInfo>> it = clientsInWork.entrySet().iterator(); it.hasNext(); ) {
//                Map.Entry<ClientInfo,TeamInfo> entry = it.next();
//                if(entry.getKey().equals(client)) {
//                    team = entry.getValue();
//                    it.remove();
//                    return team;
//                }
//            }
//            return null;
//        //team.addToNumber();
//    }
//    public void addInQueue(ClientInfo clientInfo) {
//        //pendingClients.add(clientInfo);
//        teamForClient.put(clientInfo,null);
//        queueOfWorkers.addLast(clientInfo);
//    }
//    public void addInWork(ClientInfo[] clients) {
//        TeamInfo team = new TeamInfo(clients);
//        for (ClientInfo client: clients) {
//            clientsInWork.put(client,team);
//        }
//    }
//    /*public ClientInfo getFromQueue() {
//        synchronized (pendingClients) {
//            if (pendingClients.size() > 0) {
//                return pendingClients.poll();
//            } else {
//                return null;//FIXME:
//            }
//        }
//    }*/
//    public List<ClientInfo> getNElemsFromQueueIfAvailable(int n) {
//        synchronized (queueOfWorkers) {
//            List<ClientInfo> toReturn=new ArrayList<>();
//            if (queueOfWorkers.size() >= n) {
//                for(int i=0;i<n;i++) {
//                    toReturn.add(i, queueOfWorkers.poll());
//                }
//                return toReturn;
//            } else {
//                return null;
//                //throw new Error("not enough items in the queue");//FIXME
//            }
//        }
//    }
//
//    public void updateTeamList() {
//        synchronized (queueOfTeams) {
//            if(queueOfTeams.get(0).clientsCount > queueOfWorkers.size()){
//                return;
//            }
//            List<ClientInfo> clients = getNElemsFromQueueIfAvailable(queueOfTeams.get(0).clientsCount);
//            queueOfTeams.get(0).clients = clients;
//        }
//    }
//}
