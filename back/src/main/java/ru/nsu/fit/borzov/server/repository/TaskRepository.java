package ru.nsu.fit.borzov.server.repository;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.borzov.server.model.TaskEntity;

import java.util.UUID;
import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<TaskEntity, UUID> {
    //@Query("select t from TaskEntity t order by t.timestamp")
    //List<TaskEntity> getFirstTaskSortedByTime(Pageable pageable);

    @Query("select t from TaskEntity t where t.isStarted = false order by t.timestamp")
    List<TaskEntity> getFirstTaskSortedByTime(Pageable pageable);

    default TaskEntity getFirstTaskSortedByTime() {
        List<TaskEntity> tasks = getFirstTaskSortedByTime(PageRequest.of(0,1));
        if(tasks.isEmpty()) {
            return null;
        }
        return tasks.get(0);
    }
}
