package ru.nsu.fit.borzov.server.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.borzov.server.model.ClientEntity;
import ru.nsu.fit.borzov.server.model.variable.Matrix;
import ru.nsu.fit.borzov.server.model.Row;
import ru.nsu.fit.borzov.server.model.TaskEntity;
import ru.nsu.fit.borzov.server.model.variable.Variable;
import ru.nsu.fit.borzov.server.repository.ClientRepository;
import ru.nsu.fit.borzov.server.repository.TaskRepository;
import ru.nsu.fit.borzov.server.view.TaskView;
//import ru.nsu.fit.borzov.server.repository.MainRepository;

import java.util.List;

import java.util.UUID;
import java.util.logging.Logger;

@Service
//@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MainService {
    //private final MainRepository repository;
    private final ClientRepository clientRepository;
    private final TaskRepository taskRepository;
    MainService(ClientRepository clientRepository, TaskRepository taskRepository) {
        this.clientRepository = clientRepository;
        this.taskRepository = taskRepository;
        clientRepository.deleteAll();
        taskRepository.deleteAll();
    }
    Logger logger = Logger.getLogger(MainService.class.getName());

    /*
    public void pickTeam(int amount) {
        repository.getNElemsFromQueueIfAvailable(amount);
    }
    */
    public void addInQueue(String clientIp, int port) throws Exception {
        if (clientIp != null && port >= 0 && port <= 65535) {
            //ClientInfo clientInfo = new ClientInfo(clientIp, port);
            //repository.addInQueue(clientInfo);
            //repository.updateTeamList();
            ClientEntity client = new ClientEntity();
            client.setIp(clientIp);
            client.setPort(port);
            clientRepository.save(client);
            updateTasks();
        } else {
            throw new Exception("ip == " + clientIp + " port == " + port);//FIXME
        }
    }

    @Transactional
    public UUID addTask(String clientIp, int port, TaskView taskView) {
        ClientEntity client = new ClientEntity();
        client.setIp(clientIp);
        client.setPort(port);
        TaskEntity task = new TaskEntity();
        task.setUsersCount(taskView.getUsersCount());
        task.setCode(taskView.getCode());
        for(Row row :taskView.getCode()) {
            row.setTask(task);
        }
        task.setContext(taskView.getContext());
        for(Variable variable :taskView.getContext()) {
            variable.setTask(task);
        }
        /*
        for(Matrix matrix :taskView.getContext()) {
            matrix.setTask(task);
        }
         */
        //task = taskRepository.save(task);

        //client.setTask(task);
        //client = clientRepository.save(client);
        client.setTask(task);
        task.getClients().add(client);
        if(task.getUsersCount() == task.getClients().size()) {
            task.setStarted(true);
        }
        task = taskRepository.save(task);
        logger.info("new task:" + task.toString());
        return task.getId();
    }

    public TaskEntity checkClientStatus(String clientIp, int port) {
        logger.info("status of client '" + clientIp + ":" + port + "'");
        ClientEntity client = clientRepository.findFirstByIpAndPort(clientIp, port);
        logger.info("client task: '" + (client.getTask() == null ? null : client.getTask().toString()));
        TaskEntity task = client.getTask();
        if(task == null) {
            return null;
        }
        task.setCurrUserNumber(task.getClients().indexOf(client));
        return task;
    }

    @Transactional
    public void updateTasks() {
        TaskEntity task = taskRepository.getFirstTaskSortedByTime();
        if(task == null) {
            return;
        }
        List<ClientEntity> clients = clientRepository.findAllByTask(null);
        if (clients.size() >= task.getUsersCount() - task.getClients().size()) {
            List<ClientEntity> taskClients = task.getClients();
            int inTask = task.getClients().size();
            for (int i = 0; i < task.getUsersCount() - inTask; i++) {
                ClientEntity client = clients.get(i);
                taskClients.add(client);
                client.setTask(task);
            }
            task.setStarted(true);
            taskRepository.save(task);
        }
    }

    public TaskEntity checkTaskStatus(UUID id, String ip, int port) {
        TaskEntity task = checkTaskStatus(id);
        ClientEntity client = clientRepository.findFirstByIpAndPort(ip,port);
        if(task == null) {
            return null;
        }
        task.setCurrUserNumber(task.getClients().indexOf(client));
        return task;
    }

    public TaskEntity checkTaskStatus(UUID id) {
        logger.info("status of taskID:" + id);
        TaskEntity task = taskRepository.getOne(id);
        logger.info("task status:" + task.toString());
        return task;
    }

    public void deleteTask(UUID id) {
        taskRepository.deleteById(id);
        logger.info("delete task: " + id);
    }
}
