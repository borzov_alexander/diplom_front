package ru.nsu.fit.borzov.server.view;

public class ClientInfo {
    public String ip;
    public int port;
    public int size;
    public int rank;
    public ClientInfo(String clientIp, int port) {
        this.ip = clientIp;
        this.port = port;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        if(obj.getClass() != this.getClass()) {
            return false;
        }
        ClientInfo another = (ClientInfo) obj;
        return another.port == port && another.ip.equals(ip);
    }
}
