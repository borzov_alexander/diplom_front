package ru.nsu.fit.borzov.server.view;

import lombok.Data;
import ru.nsu.fit.borzov.server.model.Row;
import ru.nsu.fit.borzov.server.model.variable.Variable;

import java.util.*;

@Data
public class TaskView {
    int usersCount;
    List<Variable> context = new ArrayList<>();
    List<Row> code = new ArrayList<>();
}
