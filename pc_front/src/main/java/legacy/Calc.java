//package parts;
//
//import java.io.IOException;
//
//public class Calc {
//    //Loger loger;
//    private int id;
//    private int recvcount;
//    private int displ;
//    private int h;
//    private double e;
//    private double t;
//    private double[] a;
//    private double[] x;
//    private double[] b;
//    private double[] y;
//    private double[] ay;
//    private double normB;
//    private int rank;
//    private int size;
//    private int[] recvcounts;
//    private int[] displs;
//    private Network net;
//
//    public Calc(Network net, int rank, int size) {
//        this.net = net;
//        this.rank = rank;
//        this.size = size;
//        recvcounts = new int[size];
//        displs = new int[size];
//        h = 1024;
//        disprec(recvcounts, displs, size, h);
//        recvcount = recvcounts[rank];
//        displ = displs[rank];
//        a = new double[h * recvcounts[0]];
//        x = new double[recvcounts[0]];
//        b = new double[recvcounts[0]];
//        y = new double[recvcounts[0]];
//        ay = new double[recvcounts[0]];
//        e = 0.00001;
//    }
//
//    void disprec(int[] recvcounts, int[] displs, int size, int h) {
//        //int remainder = h % size;
//        //int integer = h / size;
//        int t = 0;
//        for (int i = 0; i < size; i++) {
//            displs[i] = t;
//            recvcounts[i] = h / size + (i < (h % size) ? 1 : 0);
//            t += recvcounts[i];
//        }
//    }
//
//    public double[] calc() throws IOException {
//        System.out.println("rank: " + rank);
//        System.out.println("size: " + size);
//        System.out.println("recvcount " + recvcounts[rank]);
//        double[] u = new double[recvcounts[0]];
//        //long start = System.nanoTime();
//        //System.out.println("a:");
//        for (int i = 0; i < recvcounts[rank]; i++) {
//            for (int j = 0; j < h; j++) {
//                a[i * h + j] = 1;
//                if (i + displs[rank] == j) {
//                    a[i * h + j] = 2;
//                }
//                //System.out.print(a[i * h + j]  + " , ");
//            }
//            //System.out.println();
//        }
//        //System.out.println();
//        //System.out.println("a END");
//        System.out.println("u:");
//        for (int i = 0; i < recvcounts[rank]; i++) {
//            u[i] = Math.sin(2 * (i + displs[rank]) * 3.14 / h);
//            System.out.println(u[i] + " , ");
//        }
//        System.out.println();
//        System.out.println("u END");
//        MatrixOps.mul(a, u, b, h, recvcounts, displs, rank, size, net);
//        /*System.out.println("b:");
//        for (int i = 0; i < b.length; i++) {
//            System.out.print(b[i]  + " , ");
//        }
//        System.out.println();*/
//        //System.out.println("b END");
//        /*for (int i = 0; i < recvcounts[rank]; i++) {
//            x[i] = 0;
//        }*/
//        //long start = System.currentTimeMillis();
//        normB = Math.sqrt(sk(b, b));
//        System.out.println("||b|| == " + normB);
//        try {
//            while (test()) {
//                MatrixOps.mul(a, y, ay, h, recvcounts, displs, rank, size, net);
//                t = sk(ay, y) / sk(ay, ay);
//                for (int i = 0; i < recvcount; i++) {
//                    x[i] -= t * y[i];
//                }
//            }
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        System.out.println("x:");
//        for (int i = 0; i < x.length; i++) {
//            System.out.println(x[i] + " , ");
//        }
//        double normax_b = calcEps(a, x, b);
//        System.out.println();
//        System.out.println("u-x:");
//        for(int i=0;i<u.length;i++) {
//            System.out.println(u[i]-x[i]);
//        }
//        System.out.println("u-xEND");
//        System.out.println("||Ax-b|| == " + normax_b +  "||Ax-b||-e == " + (normax_b - e));
//        /*if(normax_b > e) {
//            System.out.println("||Ax-b|| > e");
//            System.out.println("ERROR!");
//        } else {
//            System.out.println("||Ax-b|| < e");
//            System.out.println("OK!");
//        }*/
//        //long end = System.nanoTime();
//        //long end = System.currentTimeMillis();
//        //loger.logText("time: " + (end - start));
//        //System.out.println("time: " + (end-start));
//        return x;
//    }
//
//    private double calcEps(double[] a, double[] x, double[] b) throws IOException {
//        double[] ax = new double[recvcount];
//        MatrixOps.mul(a, x, ax, h, recvcounts, displs, rank, size, net);
//        double[] ax_b = new double[recvcount];
//        for (int i = 0; i < recvcount; i++) {
//            ax_b[i] = ax[i] - b[i];
//        }
//        double result = 0;
//        result = Math.sqrt(sk(ax_b,ax_b));
//        return result;
//    }
//
//    double sk(double[] A, double[] B) {
//        double result = 0;
//        for (int i = 0; i < A.length; i++) {
//            result += A[i] * B[i];
//        }
//        try {
//            result = net.AllReduce(result, rank, size);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        return result;
//    }
//
//    boolean test() throws IOException {
//        MatrixOps.mul(a, x, y, h, recvcounts, displs, rank, size, net);
//        for (int i = 0; i < recvcount; i++) {
//            y[i] -= b[i];
//        }
//        double v = Math.sqrt(sk(y, y)) / normB;
//        if (v < e) {
//            return false;
//        }
//        System.out.println("skr / normB == " + v);
//        return true;
//    }
//}
