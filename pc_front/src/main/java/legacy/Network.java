//package parts;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import parts.view.AddTaskView;
//import parts.view.Client;
//import parts.view.Task;
//
//import java.io.*;
//import java.net.HttpURLConnection;
//import java.net.ServerSocket;
//import java.net.Socket;
//import java.net.URL;
//import java.nio.ByteBuffer;
//import java.util.ArrayList;
//import java.util.UUID;
//
//public class Network {
//    Socket prev;
//    ServerSocket serverSocket;
//    Socket next;
//    String url;
//    int port;
//
//    Network(String url, int port) {
//        this.url = url;
//        this.port = port;
//    }
//
//    Task checkClientStatus() throws IOException {
//        String result = sendToBase(url + "/checkClientStatus/"  + port, "GET", null);
//        if(result == null) {
//            return null;
//        }
//        StringReader reader = new StringReader(result);
//        ObjectMapper mapper = new ObjectMapper();
//        return mapper.readValue(reader, Task.class);
//    }
//
//    public void connectToNeighbourFromTask(Task task) {
//        int size = task.getUsersCount();
//        int rank = task.getCurrUserNumber();
//        try {
//            //FIXME: соседи в классе сети это странно.
//            Client neighbour = task.getClients().get((rank + 1) % size);
//            connectToNeighbour(neighbour);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public Task getTaskFromServer() throws IOException {
//        System.out.println("send: " + url + "/addInQueue/" + port);
//        sendToBase(url + "/addInQueue/" + port, "POST", null);
//        Task task = checkClientStatus();
//        try {
//            while (task == null) {
//                Thread.sleep(1000);
//                task = checkClientStatus();
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();//TODO:
//        }
//
//        //System.out.println("ip: " + neighbour.ip + ":" + neighbour.port + " rank: " + neighbour.rank + " size: " + neighbour.size);
//        //connectToNeighbourFromTask(task);
//        return task;
//    }
//
//    private void connectToNeighbour(Client client) throws IOException {
//        serverSocket = new ServerSocket(port);
//        while (true) {
//            try {
//                prev = new Socket(client.getIp(), client.getPort());
//                break;
//            } catch (IOException e) {
//                System.out.println("refused...");
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException ex) {
//                    ex.printStackTrace();
//                }
//            }
//        }
//        next = serverSocket.accept();
//    }
//
//    private void connectToNeighbours(String[] ips, int[] port) throws IOException {
//        serverSocket = new ServerSocket(port[0]);
//        while (true) {
//            try {
//                prev = new Socket(ips[0], port[1]);
//                break;
//            } catch (IOException e) {
//                System.out.println("refused...");
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException ex) {
//                    ex.printStackTrace();
//                }
//            }
//        }
//        next = serverSocket.accept();
//    }
//
//    private void send(double[] arr, int[] recvcounts, int[] displs, int part, int rank, int size, int h) throws IOException {
//        double[] tmp = new double[recvcounts[rank]];
//        if (rank % 2 == 0) {
//            MPI_Ssend(arr, recvcounts[part], next);//(rank + 1) % size
//            MPI_Recv(tmp, recvcounts[(part - 1 + size) % size], prev); //(rank - 1 + size) % size
//        } else {
//            MPI_Recv(tmp, recvcounts[(part - 1 + size) % size], prev); //(rank - 1 + size) % size
//            MPI_Ssend(arr, recvcounts[part], next); //(rank + 1) % size
//        }
//        for (int i = 0; i < tmp.length; i++) {//FIXME: почему-то было h / size + 1
//            arr[i] = tmp[i];
//        }
//    }
//
//    private void MPI_Ssend(double[] arr, int recvcount, Socket destination) throws IOException {
//        //писать в destination
//        byte[] toWrite = new byte[arr.length * Double.BYTES];
//        for (int i = 0; i < arr.length; i++) {
//            byte[] b1 = toByteArray(arr[i]);
//            System.arraycopy(b1, 0, toWrite, i * Double.BYTES, Double.BYTES);
//        }
//        destination.getOutputStream().write(toWrite);
//    }
//
//    private void MPI_Recv(double[] arr, int recvcount, Socket sender) throws IOException {
//        //читать из sender
//        byte[] toRead = new byte[recvcount * Double.BYTES];
//        int size = 0;
//        while (size != recvcount * Double.BYTES) {
//            size += sender.getInputStream().read(toRead, size, toRead.length - size);
//            //System.out.println("size: " + size);
//        }
//        //System.out.println("received");
//        //int size = sender.getInputStream().read(toRead,0,toRead.length);
//        if (size != recvcount * Double.BYTES) {
//            try {
//                throw new Exception("recived = " + size + " excepted " + recvcount * Double.BYTES);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        for (int i = 0; i < toRead.length / Double.BYTES; i++) {
//            arr[i] = toDouble(toRead, Double.BYTES * i);
//        }
//    }
//
//    private static byte[] toByteArray(double value) {
//        byte[] bytes = new byte[8];
//        ByteBuffer.wrap(bytes).putDouble(value);
//        return bytes;
//    }
//
//    private static double toDouble(byte[] bytes) {
//        return ByteBuffer.wrap(bytes).getDouble();
//    }
//
//    private static double toDouble(byte[] bytes, int startPos) {
//        byte[] b = new byte[Double.BYTES];
//        System.arraycopy(bytes, startPos, b, 0, b.length);
//        return ByteBuffer.wrap(b).getDouble();
//    }
//
//    public double AllReduce(double value, int rank, int size) throws IOException {
//        double[] tmp = new double[1];
//        double[] results = new double[size];
//        double summ = 0;
//        for (int i = 0; i < size; i++) {
//            if (rank % 2 == 0) {
//                MPI_Ssend(new double[]{value}, 1, next);
//                MPI_Recv(tmp, 1, prev); //(rank - 1 + size) % size
//            } else {
//                MPI_Recv(tmp, 1, prev); //(rank - 1 + size) % size
//                MPI_Ssend(new double[]{value}, 1, next);
//            }
//            value = tmp[0];
//            results[(i + rank) % size] = tmp[0];
//        }
//        for (int i = 0; i < results.length; i++) {
//            summ += results[i];
//        }
//        return summ;
//    }
//
//    private String sendToBase(String urlString, String method, String body) {
//        System.out.println("send to Base: " + urlString + "; " + method + "; " + body);
//        URL url;
//        HttpURLConnection connection = null;
//        try {
//            url = new URL(urlString);
//            connection = (HttpURLConnection) url.openConnection();
//            if (!method.equals("GET")) {
//                connection.setDoOutput(true);
//            }
//            connection.setRequestMethod(method);
//            connection.setDoInput(true);
//            connection.setRequestProperty("Content-Type", "application/json");
//            connection.setRequestProperty("Accept", "application/json");
//            if (connection.getDoOutput()) {
//                OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
//                try {
//                    /*JSONObject abtest = new JSONObject();
//                    abtest.put("a",new JSONArray(new int[]{9,8,7,6,5,4,3,2,1}));
//                    abtest.put("b",new JSONArray(new int[]{1,2,3,4,5,6,7,8,9}));*/
//                    if (body != null) {
//                        wr.write(body);
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    wr.flush();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (connection.getInputStream() != null) {
//                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                String line = br.readLine();
//                return line;
//            }
//            //callback.accept(line);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (connection != null) {
//                connection.disconnect();
//            }
//        }
//        return null;
//    }
//
//    public UUID addTast(AddTaskView taskView) throws IOException {
//        ObjectMapper mapper = new ObjectMapper();
//        String objJson = mapper.writeValueAsString(taskView);
//
//        String result = sendToBase(url + "/addTask/" + port, "POST",objJson);
//        StringReader reader = new StringReader(result);
//        UUID taskId = mapper.readValue(reader, UUID.class);
//        //System.out.println("ip: " + neighbour.ip + ":" + neighbour.port + " rank: " + neighbour.rank + " size: " + neighbour.size);
//        return taskId;
//    }
//
//    public Task checkTaskStatus(UUID id) throws IOException {
//        ObjectMapper mapper = new ObjectMapper();
//        String objJson = sendToBase(url + "/checkTaskStatus/" + id.toString(), "GET",null);
//        if(objJson == null) {
//            return null;
//        }
//        return mapper.readValue(objJson,Task.class);
//    }
//}
//
