package parts;

import parts.lang.code.TaskCreator;
import parts.lang.variable.Matrix;
import parts.lang.code.Row;
import parts.lang.variable.MyDouble;
import parts.lang.variable.Variable;
import parts.view.AddTaskView;

import java.util.*;
import java.io.IOException;
import java.util.ArrayList;

//аргументы для 3
//http://localhost:8080 732
//http://localhost:8080 733
//http://localhost:8080 734
public class Test {
    public static void main(String[] args) throws IOException {
        test(args);
        //simpleScalarTest(args);
        System.out.println("bye");
    }

    static void timeTest(String[] args) throws IOException {
        IncredibleLib lib = new IncredibleLib(args[0], Integer.parseInt(args[1]));
        if (Integer.parseInt(args[1]) == 732) { //для тестов, чтобы выделить создателя работы
            AddTaskView addTaskView = new AddTaskView();
            addTaskView.setUsersCount(3);

            List<Row> code = new ArrayList<>();
            Matrix[] m = new Matrix[3];
            m[0] = new Matrix(100, 100);
            m[1] = new Matrix(100, 100);
            m[2] = new Matrix(100, 100);
            for (int i = 0; i < 100 * 100; i++) {
                m[1].data[i] = i % 100 + 1;
                m[2].data[i] = (i * 3) % 100 + 1;
            }
            List<Variable> context = new ArrayList<>();
            context.add(new Variable(m[0]));
            context.add(new Variable(m[1]));
            context.add(new Variable(m[2]));
            for (int i = 0; i < 15; i++) {
                Row row = new Row();
                row.operation = 1;
                row.argsNumbers = new int[]{0, 1, 2};//{i%3,(i+1)%3,(i+2)%3};
                code.add(row);
            }
            addTaskView.setCode(code);
            addTaskView.setContext(context);
            lib.doMyWork(addTaskView);
        } else {
            lib.doElseWork();
        }
    }

    static void simpleMulTest(String[] args) throws IOException {
        IncredibleLib lib = new IncredibleLib(args[0], Integer.parseInt(args[1]));
        if (Integer.parseInt(args[1]) == 732) { //для тестов, чтобы выделить создателя работы
            AddTaskView addTaskView = new AddTaskView();
            addTaskView.setUsersCount(3);

            List<Row> code = new ArrayList<>();
            Matrix[] m = new Matrix[3];
            m[0] = new Matrix(3, 3);
            m[1] = new Matrix(new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, 3);
            m[2] = new Matrix(new double[]{11, 12, 13, 14, 15, 16, 17, 18, 19}, 3);
            List<Variable> context = new ArrayList<>();
            context.add(new Variable(m[0]));
            context.add(new Variable(m[1]));
            context.add(new Variable(m[2]));
            Row row = new Row();
            row.operation = 1;
            row.argsNumbers = new int[]{0, 1, 2};
            code.add(row);
            addTaskView.setCode(code);
            addTaskView.setContext(context);
            lib.doMyWork(addTaskView);
        } else {
            lib.doElseWork();
        }
    }

    static void simpleScalarTest(String[] args) throws IOException {
        IncredibleLib lib = new IncredibleLib(args[0], Integer.parseInt(args[1]));
        if (Integer.parseInt(args[1]) == 732) { //для тестов, чтобы выделить создателя работы
            AddTaskView addTaskView = new AddTaskView();
            addTaskView.setUsersCount(3);

            List<Row> code = new ArrayList<>();
            Matrix[] m = new Matrix[2];
            m[0] = new Matrix(new double[]{1, 2, 3}, 3);
            m[1] = new Matrix(new double[]{4, 5, 6}, 3);
            List<Variable> context = new ArrayList<>();
            context.add(new Variable(new MyDouble(0)));
            context.add(new Variable(m[0]));
            context.add(new Variable(m[1]));
            Row row = new Row();
            row.operation = 2;
            row.argsNumbers = new int[]{0, 1, 2};
            code.add(row);
            addTaskView.setCode(code);
            addTaskView.setContext(context);
            lib.doMyWork(addTaskView);
        } else {
            lib.doElseWork();
        }
    }

    static void simpleRectangleMulTest(String[] args) throws IOException {
        IncredibleLib lib = new IncredibleLib(args[0], Integer.parseInt(args[1]));
        if (Integer.parseInt(args[1]) == 732) { //для тестов, чтобы выделить создателя работы
            AddTaskView addTaskView = new AddTaskView();
            addTaskView.setUsersCount(3);

            List<Row> code = new ArrayList<>();
            Matrix[] m = new Matrix[3];
            m[0] = new Matrix(3, 1);
            m[1] = new Matrix(new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, 3);
            m[2] = new Matrix(new double[]{11, 12, 13}, 1);
            List<Variable> context = new ArrayList<>();
            context.add(new Variable(m[0]));
            context.add(new Variable(m[1]));
            context.add(new Variable(m[2]));
            Row row = new Row();
            row.operation = 1;
            row.argsNumbers = new int[]{0, 1, 2};
            code.add(row);
            addTaskView.setCode(code);
            addTaskView.setContext(context);
        } else {
            lib.doElseWork();
        }
    }

    static void test(String[] args) throws IOException {
        IncredibleLib lib = new IncredibleLib(args[0], Integer.parseInt(args[1]));
        if (Integer.parseInt(args[1]) == 732) { //для тестов, чтобы выделить создателя работы
            TaskCreator creator = new TaskCreator();
            int h = 5;
            Matrix a = new Matrix(h, h);
            Matrix u = new Matrix(h, 1);

            for (int i = 0; i < a.height; i++) {
                for (int j = 0; j < a.width; j++) {
                    a.data[i * a.width + j] = 1;
                    if (i == j) {
                        a.data[i * a.width + j] = 2;
                    }
                }
            }
            //System.out.println(a.toString());
            //System.out.println("...........");
            for (int i = 0; i < u.height; i++) {
                u.data[i] = Math.sin(2 * i * 3.14 / u.height);//Math.PI
            }

            //System.out.println(u.toString());
            //System.out.println("...........");

            creator.addVar("a", new Variable(a));
            creator.addVar("b", new Variable(new Matrix(h, 1)));
            creator.addVar("u", new Variable(u));
            creator.addVar("x", new Variable(new Matrix(h, 1)));
            creator.addVar("y", new Variable(new Matrix(h, 1)));
            creator.addVar("ay", new Variable(new Matrix(h, 1)));
            creator.addVar("normB", new Variable(new MyDouble()));
            creator.addVar("t1", new Variable(new MyDouble()));
            creator.addVar("t2", new Variable(new MyDouble()));
            creator.addVar("t", new Variable(new MyDouble()));

            creator.op("mul", new String[]{"b", "a", "u"});

            creator.op("scalar", new String[]{"normB", "b", "b"});
            creator.op("sqrt", new String[]{"normB", "normB"});

            for (int i = 0; i < 10; i++) {
                //test start
                creator.op("mul", new String[]{"y", "a", "x"});
                creator.op("sub", new String[]{"y", "y", "b"});
                //creator.op("scalar", new String[]{"t", "y", "y"});//t tmp
                //creator.op("sqrt", new String[]{"t", "t"});//t test
                //creator.op("divdd", new String[]{"t", "t", "normB"});
                //test end
                creator.op("mul", new String[]{"ay", "a", "y"});
                creator.op("scalar", new String[]{"t1", "ay", "y"});
                creator.op("scalar", new String[]{"t2", "ay", "ay"});
                creator.op("divdd", new String[]{"t", "t1", "t2"});
                creator.op("muld", new String[]{"y", "y", "t"}); //y = t * y
                creator.op("sub", new String[]{"x", "x", "y"});
            }

            Variable[] results = lib.doMyWork(creator.buildTask(3));
            System.out.println("result x:");
            System.out.println(results[creator.getVarNumber("x")].getMatrix().toString());
        } else {
            lib.doElseWork();
        }
    }
}
