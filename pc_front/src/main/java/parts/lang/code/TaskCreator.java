package parts.lang.code;

import parts.lang.ops.OpsFactory;
import parts.lang.variable.Variable;
import parts.view.AddTaskView;

import java.util.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class TaskCreator {
    Map<String, Integer> vars = new HashMap<>();
    Map<String, Integer> ops = new HashMap<>();
    List<Row> code = new LinkedList<>();
    List<Variable> context = new LinkedList<>();
    AddTaskView addTaskView = new AddTaskView();

    public TaskCreator() {
        ops = OpsFactory.getNameToIdMap();
    }

    public TaskCreator addVar(String name, Variable variable) {
        vars.put(name, vars.size());
        context.add(variable);
        return this;
    }

    public TaskCreator op(String op, String[] argNames) {
        int[] args = new int[argNames.length];
        for (int i = 0; i < argNames.length; i++) {
            String s = argNames[i];
            int argNamesV = vars.get(s);
            args[i] = vars.get(argNames[i]);
        }
        int opNumber = ops.get(op);
        code.add(new Row(opNumber, args));
        return this;
    }

    public AddTaskView buildTask(int clients) {
        addTaskView.setUsersCount(clients);
        addTaskView.setCode(code);
        addTaskView.setContext(context);
        return addTaskView;
    }

    public int getVarNumber(String name) {
        return vars.get(name);
    }
}
