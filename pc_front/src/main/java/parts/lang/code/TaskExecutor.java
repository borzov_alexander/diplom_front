package parts.lang.code;

import parts.lang.code.Row;
import parts.lang.ops.Operation;
import parts.lang.ops.OpsFactory;
import parts.lang.variable.Variable;
import parts.network.TaskNet;
import parts.view.Task;

import java.time.Duration;
import java.time.Instant;

public class TaskExecutor {
    TaskNet taskNet;
    private final Operation[] operations;
    Variable[] context;
    Row[] code;

    public TaskExecutor(Task task, TaskNet taskNet) {
        this.taskNet = taskNet;
        context = task.getContext();
        code = task.getCode();
        operations = OpsFactory.createOps(taskNet);
    }

    public Variable[] run() {
        Instant start = Instant.now();
        for (int i = 0; i < code.length; i++) {
            Row row = code[i];
            Variable[] args = getArgs(row.argsNumbers);
            getOperation(row.operation).run(args);
            /*
            System.out.println(
                    "row: " + i +
                    "; op: " + getOperation(row.operation).getClass().getName() +
                    "; result:");
            System.out.println((args[0].getMatrix().width != 0)
                    ? args[0].getMatrix().toString()
                    : args[0].getDoubleObj().getValue());
            System.out.println("...........");
             */
        }
        Instant finish = Instant.now();
        long elapsed = Duration.between(start, finish).toMillis();
        System.out.println("Прошло времени, мс: " + elapsed);
        return context;
    }

    Operation getOperation(int operationNumber) {
        return operations[operationNumber];
    }

    private Variable[] getArgs(int[] argsNumbers) {
        Variable[] toReturn = new Variable[argsNumbers.length];
        for (int i = 0; i < toReturn.length; i++) {
            toReturn[i] = context[argsNumbers[i]];
        }
        return toReturn;
    }
}
