package parts.lang.ops;

import parts.lang.variable.MyDouble;
import parts.lang.variable.Variable;
import parts.network.TaskNet;

public class DivDD implements Operation {
    private final TaskNet taskNet;

    private int id;

    public DivDD(TaskNet taskNet) {
        this.taskNet = taskNet;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void run(Variable[] args) {
        MyDouble result = args[0].getDoubleObj();
        MyDouble a = args[1].getDoubleObj();
        MyDouble b = args[2].getDoubleObj();
        result.setValue(a.getValue() / b.getValue());
    }
}
