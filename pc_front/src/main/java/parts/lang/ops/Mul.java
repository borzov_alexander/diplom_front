package parts.lang.ops;

import parts.lang.variable.Matrix;
import parts.lang.variable.Variable;
import parts.network.TaskNet;

import java.io.IOException;

public class Mul implements Operation {
    private final TaskNet taskNet;

    private int id;

    public Mul(TaskNet taskNet) {
        this.taskNet = taskNet;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void run(Variable[] args) {
        Matrix result = args[0].getMatrix();
        Matrix a = args[1].getMatrix();
        Matrix b = args[2].getMatrix();
        int rank = taskNet.getRank();
        int size = taskNet.getSize();
        int part = rank;
        int[] recvcounts = new int[size];
        int[] displs = new int[size];
        int t = 0;
        int aw = a.width;
        int bw = b.width;
        int ah = a.height;
        for (int i = 0; i < size; i++) {
            displs[i] = t;
            recvcounts[i] = ah / size + (i < (ah % size) ? 1 : 0);
            t += recvcounts[i];
        }
        /*
        System.out.print("recvcounts:");
        for (int i = 0; i < recvcounts.length; i++) {
            System.out.print(recvcounts[i] + " ");
        }
        System.out.print("displs:");
        for (int i = 0; i < displs.length; i++) {
            System.out.print(displs[i] + " ");
        }
        //System.out.println("mul start:");
        /*
        System.out.println("start:");
        System.out.println("A:");
        System.out.println(a.toString());
        System.out.println("start:");
        System.out.println("b:");
        System.out.println(b.toString());
        System.out.println("start:");
        System.out.println("r:");
        System.out.println(result.toString());
         */
        //for (int p = 0; p < size; p++) {
        //System.out.println("mul");
        //System.out.println("b:");
        //System.out.println(b.toString());

        for (int i = displs[rank]; i < displs[rank] + recvcounts[rank]; i++) {
            for (int j = 0; j < b.width; j++) {
                result.data[i * bw + j] = 0;
                for (int k = 0; k < a.width; k++) {
                    /*
                    System.out.println("****************");
                    System.out.println("i: " + i + " j: " + j + " k: " + k);
                    System.out.println("[" + i + ", " + j + "] = a[" + i + ", " + k + "] * b[" + k + ", " + j + "]");
                    System.out.println("[" + i + ", " + j + "] = " + a.data[aw * i + k] + " * " + b.data[k * bw + j]);
                    */
                    result.data[i * bw + j] += a.data[aw * i + k] * b.data[k * bw + j];

                }
                //System.out.println("SUM:[" + i + ", " + j + "] = " + result.data[i * bw + j]);
            }
        }
        //System.out.println("....................");
        //System.out.println();
        //System.out.println("before send:");
        //System.out.println(result.toString());
//            try {
//                //taskNet.send(result, part, displs, recvcounts);
//                /*
//                if (rank % 2 == 0) {
//                    taskNet.MPI_Ssend(b.width, b.data, displs[part], recvcounts[part], taskNet.next);//(rank + 1) % size
//                    taskNet.MPI_Recv(b.width, b.data, displs[part], recvcounts[part], taskNet.prev); //(rank - 1 + size) % size
//                } else {
//                    taskNet.MPI_Recv(b.width, b.data, displs[part], recvcounts[part], taskNet.prev); //(rank - 1 + size) % size
//                    taskNet.MPI_Ssend(b.width, b.data, displs[part], recvcounts[part], taskNet.next); //(rank + 1) % size
//                }
//                 */
//                taskNet.send(b, part, displs, recvcounts);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        //System.out.println("after send:");
        //System.out.println(result.toString());
        //part = (part - 1 + size) % size;
        //}
        try {
            taskNet.MPI_ALLGATHERV(result.width, result.data, displs, recvcounts);
        } catch (IOException e) {
            e.printStackTrace();//FIXME:
        }
    }
}
