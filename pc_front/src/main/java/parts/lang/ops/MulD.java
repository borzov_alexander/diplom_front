package parts.lang.ops;

import parts.lang.variable.Matrix;
import parts.lang.variable.Variable;
import parts.network.TaskNet;

import java.io.IOException;

public class MulD implements Operation {
    private final TaskNet taskNet;

    private int id;

    public MulD(TaskNet taskNet) {
        this.taskNet = taskNet;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void run(Variable[] args) {
        Matrix result = args[0].getMatrix();
        Matrix a = args[1].getMatrix();
        double d = args[2].getDoubleObj().getValue();
        int rank = taskNet.getRank();
        int size = taskNet.getSize();
        int part = rank;
        int[] recvcounts = new int[size];
        int[] displs = new int[size];
        int t = 0;
        int aw = a.width;
        int ah = a.height;
        for (int i = 0; i < size; i++) {
            displs[i] = t;
            recvcounts[i] = ah / size + (i < (ah % size) ? 1 : 0);
            t += recvcounts[i];
        }

        for (int i = displs[rank]; i < displs[rank] + recvcounts[rank]; i++) {
            for (int j = 0; j < aw; j++) {
                result.data[i * aw + j] = d * a.data[i * aw + j];
            }
        }
        try {
            taskNet.MPI_ALLGATHERV(result.width, result.data, displs, recvcounts);
        } catch (IOException e) {
            e.printStackTrace();//FIXME:
        }
    }
}
