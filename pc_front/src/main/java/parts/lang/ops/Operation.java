package parts.lang.ops;

import parts.lang.variable.Variable;

public interface Operation {

    int getId();

    void setId(Integer id);

    void run(Variable[] argsContainer);
}
