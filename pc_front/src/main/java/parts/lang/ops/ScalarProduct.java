package parts.lang.ops;

import parts.lang.variable.MyDouble;
import parts.lang.variable.Matrix;
import parts.lang.variable.Variable;
import parts.network.TaskNet;

import java.io.IOException;

public class ScalarProduct implements Operation {
    private final TaskNet taskNet;

    private int id;

    public ScalarProduct(TaskNet taskNet) {
        this.taskNet = taskNet;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public void run(Variable[] args) {
        /*
        for (int i = 1; i < 3; i++) {
            if (!(args[i] instanceof Matrix)) {
                throw new IllegalArgumentException("expected " + Matrix.class.getName());
            }
        }
        if (!(args[0] instanceof MyDouble)) {
            throw new IllegalArgumentException("expected " + MyDouble.class.getName());
        }
        */

        MyDouble result = args[0].getDoubleObj();
        Matrix aMatrix = args[1].getMatrix();
        Matrix bMatrix = args[2].getMatrix();
        int rank = taskNet.getRank();
        int size = taskNet.getSize();
        int[] recvcounts = new int[size];
        int[] displs = new int[size];
        int t = 0;
        int aw = aMatrix.width;
        int bw = bMatrix.width;
        int ah = aMatrix.height;
        for (int i = 0; i < size; i++) {
            displs[i] = t;
            recvcounts[i] = ah / size + (i < (ah % size) ? 1 : 0);
            t += recvcounts[i];
        }
        double[] a = aMatrix.data;
        double[] b = bMatrix.data;
        double resultValue = 0;
        for (int i = displs[rank]; i < displs[rank] + recvcounts[rank]; i++) {
            resultValue += a[i] * b[i];
        }
        try {
            resultValue = taskNet.AllReduce(resultValue);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        result.value=resultValue;
    }
}
