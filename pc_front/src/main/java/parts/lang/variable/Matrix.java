package parts.lang.variable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Matrix {
    //public Part part = null;
    public double[] data;
    public int width;
    public int height;

    public Matrix(double[] data, int width) {
        this.data = data;
        this.width = width;
        if (data.length % width != 0) {
            throw new IllegalArgumentException("size % width not Integer");
        }
        height = data.length / width;
    }

    public Matrix(int height, int width) {
        this.width = width;
        this.height = height;
        data = new double[height * width];
        //матрица заполнена стандартным значением 0
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                builder.append(data[width * i + j]).append(" ");
            }
            builder.append(System.lineSeparator());
        }
        return builder.toString();
    }
}
