package parts.lang.variable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class MyDouble {
    public double value = 0;
    public MyDouble(double value) {
        this.value=value;
    }
    @Override
    public String toString() {
        return Double.toString(value);
    }
}
