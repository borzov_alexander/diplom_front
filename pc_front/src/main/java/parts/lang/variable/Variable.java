package parts.lang.variable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Variable {
    MyDouble doubleObj;

    Matrix matrix;

    public Variable(Matrix matrix) {
        this.matrix=matrix;
    }

    public Variable(MyDouble doubleObj) {
        this.doubleObj = doubleObj;
    }

}
