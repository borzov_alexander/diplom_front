package parts.network;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import parts.view.AddTaskView;
import parts.view.Client;
import parts.view.Task;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.*;

public class Server {
    String url;
    int port;

    public Server(String url, int port) {
        this.url = url;
        this.port = port;
    }

    Task checkClientStatus() throws IOException {
        String result = sendToBase(url + "/checkClientStatus/"  + port, "GET", null);
        if(result == null) {
            return null;
        }
        StringReader reader = new StringReader(result);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(reader, Task.class);
    }

    public Task getTaskFromServer() throws IOException {
        System.out.println("send: " + url + "/addInQueue/" + port);
        sendToBase(url + "/addInQueue/" + port, "POST", null);
        Task task = checkClientStatus();
        try {
            while (task == null) {
                Thread.sleep(1000);
                task = checkClientStatus();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();//TODO:
        }

        //System.out.println("ip: " + neighbour.ip + ":" + neighbour.port + " rank: " + neighbour.rank + " size: " + neighbour.size);
        //connectToNeighbourFromTask(task);
        return task;
    }


    private String sendToBase(String urlString, String method, String body) {
        System.out.println("send to Base: " + urlString + "; " + method + "; " + body);
        URL url;
        HttpURLConnection connection = null;
        try {
            url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            if (!method.equals("GET")) {
                connection.setDoOutput(true);
            }
            connection.setRequestMethod(method);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            if (connection.getDoOutput()) {
                OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                try {
                    /*JSONObject abtest = new JSONObject();
                    abtest.put("a",new JSONArray(new int[]{9,8,7,6,5,4,3,2,1}));
                    abtest.put("b",new JSONArray(new int[]{1,2,3,4,5,6,7,8,9}));*/
                    if (body != null) {
                        wr.write(body);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    wr.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (connection.getInputStream() != null) {
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = br.readLine();
                return line;
            }
            //callback.accept(line);
        } catch (ConnectException ce) {
            return null;//TODO:
        }
        catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }

    public UUID addTast(AddTaskView taskView) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String objJson = mapper.writeValueAsString(taskView);

        String result = sendToBase(url + "/addTask/" + port, "POST",objJson);
        //FIXME:NULL
        StringReader reader = new StringReader(result);
        UUID taskId = mapper.readValue(reader, UUID.class);
        //System.out.println("ip: " + neighbour.ip + ":" + neighbour.port + " rank: " + neighbour.rank + " size: " + neighbour.size);
        return taskId;
    }

    public Task checkTaskStatus(UUID id) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String objJson = sendToBase(url + "/checkTaskStatus/" + id.toString() + "/" + port, "GET",null);
        if(objJson == null) {
            return null;
        }
        return mapper.readValue(objJson,Task.class);
    }
    public void deleteTask(UUID id) {
        sendToBase(url + "/deleteTask/" + id.toString(), "DELETE",null);
        return;
    }
}

