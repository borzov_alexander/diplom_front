package parts.view;

import lombok.Data;

import java.util.UUID;

@Data
public class Client {
    UUID id;
    int port;
    String ip;
    Task task;
}
