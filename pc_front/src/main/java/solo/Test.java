package solo;

import java.time.Duration;
import java.time.Instant;

public class Test {
    public static void main(String[] args) {
        double[] a = new double[100 * 100];
        double[] b = new double[100 * 100];
        double[] result = new double[100 * 100];
        int aw = 100, bw = 100;
        for(int i = 0; i < 100*100;i++) {
            a[i]=i%100 + 1;
            b[i]=(i*3)%100 + 1;
        }
        Instant start = Instant.now();
        for(int count=0;count<1000;count++) {
            for (int i = 0; i < 100; i++) {
                for (int j = 0; j < 100; j++) {
                    for (int k = 0; k < 100; k++) {
                        //System.out.println("[" + i + ", " + j + "] = " + a.data[aw * i + k] + " * " + b.data[k*bw + j]);
                        result[i * aw + j] += a[aw * i + k] * b[k * bw + j];
                    }
                }
            }
        }
        Instant finish = Instant.now();
        long elapsed = Duration.between(start, finish).toMillis();
        System.out.println("Прошло времени, мс: " + elapsed);
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                System.out.print(result[i*bw+j] + " ");
            }
            System.out.println();
        }
    }
}
